#pragma once

#include <iostream>

class Money
{
private:
	int euros, centimes;

	//Creating method that prevent a value bigger than 100 to centimes
	Money checkMoney(const Money *pMoney);

public:
	Money();
	Money(int pEuros, int pCentimes);
	Money(const Money &pMoney);

	//Creating getters and setters
	int getEuros();
	void setEuros(int pEuros);
	int getCentimes();
	void setCentimes(int pCentimes);

	//Overloading Inut/Output operators
	friend std::ostream &operator<<(std::ostream &output, const Money &pMoney);
	friend std::istream &operator >> (std::istream  &input, Money &pMoney);

	//Overloading adding, subtraction, division, multiply and comparison operators
	Money operator+(const Money &pMoney);
	Money operator-(const Money &pMoney);
	Money operator*(const Money &pMoney);
	Money operator/(const Money &pMoney);
	//In case you want to divide a sum of money to certain number
	Money operator/(int pTimes);
	bool operator<(const Money &pMoney);
	bool operator>(const Money &pMoney);
	bool operator==(const Money &pMoney);
	bool operator!=(const Money &pMoney);
};
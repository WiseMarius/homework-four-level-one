#include <iostream>

#include "Money.h"

void checkOperators(Money *myMoney, Money *yourMoney)
{
	std::cout << "I have " << *myMoney << " money" << std::endl;
	std::cout << "You have " << *yourMoney << " money" << std::endl << std::endl;

	if (*myMoney > *yourMoney)
	{
		std::cout << "I have more money than you :D" << std::endl;
		std::cout << "I have " << *myMoney - *yourMoney << " more money than you" << std::endl << std::endl;
	}
	else
	{
		std::cout << "You have more money than me :(" << std::endl;
		std::cout << "You have " << *yourMoney - *myMoney << " more money than me" << std::endl << std::endl;
	}

	std::cout << "Together we have " << (*yourMoney + *myMoney) << std::endl << std::endl;

	std::cout << "Checking multiply operator: " << *myMoney * *yourMoney << std::endl;
	std::cout << "Checking division operator: " << *myMoney / *yourMoney << std::endl << std::endl;

	std::cout << "Checking example: " << *yourMoney + *yourMoney + *yourMoney << std::endl << std::endl;

	std::cout << (*yourMoney != *myMoney) << std::endl;
	*yourMoney = *myMoney;
	std::cout << (*yourMoney == *myMoney) << std::endl;
}

void main()
{
	Money *myMoney = new Money(90, 20);
	Money *yourMoney = new Money(2, 99);

	checkOperators(myMoney, yourMoney);
}